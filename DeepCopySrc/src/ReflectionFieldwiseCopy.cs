using System;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Collections;

namespace DeepCopySrc.src
{
    public class ReflectionFieldWiseCopy
    {
        static ReflectionFieldWiseCopy() { }
        private static IList<object> originalObjects;
        private static IList<object> copiedObjects;

        public static T Copy<T>(T source, T template) {
            Type objType = template.GetType();
            originalObjects = new List<object>();
            copiedObjects = new List<object>();

            var fieldCopy = CopyFieldWise(template, objType);
            return (T) fieldCopy;
        }

        protected static T CopySingleValueFieldWise<T>(T template, Type objType) {
            object clone = objType.IsAbstract ? FormatterServices.GetUninitializedObject(template.GetType()) : FormatterServices.GetUninitializedObject(objType);

            #region Handle references
            if (objType.IsClass) {
                var fo = originalObjects.FirstOrDefault(o => object.ReferenceEquals(o, template));
                if (fo != null) {
                    var index = originalObjects.IndexOf(fo);
                    return (T) copiedObjects.ElementAt(index);
                } else {
                    originalObjects.Add(template);
                    copiedObjects.Add(clone);
                }
            }
            #endregion

            FieldInfo [] fieldInfos = Helpers.GetFieldInfos(
              objType, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance
            );
            if (fieldInfos.Length == 0) {
                copiedObjects [copiedObjects.Count - 1] = template;
                return (T) (object) template;
            }
            for (int index = 0; index < fieldInfos.Length; ++index) {
                FieldInfo fieldInfo = fieldInfos [index];
                Type fieldType = fieldInfo.FieldType;

                object originalValue = fieldInfo.GetValue(template);
                if (originalValue != null) {
                    if (fieldType.IsPrimitive || (fieldType == typeof(string))) {
                        fieldInfo.SetValue(clone, originalValue);
                    } else if (fieldType.IsArray) {
                        fieldInfo.SetValue(clone, CopyArrayFieldWise(originalValue, fieldType.GetElementType()));
                    } else {
                        var value = CopyFieldWise(originalValue, fieldType);
                        fieldInfo.SetValue(clone, value);
                    }
                }
            }
            return (T) clone;
        }

        private static object CopyFieldWise(object template, Type objType) {
            if (objType.IsPrimitive || (objType == typeof(string))) {
                return template;
            } else if (objType.GetInterfaces().Any(x => x == typeof(IEnumerable)) && objType.IsGenericType) {
                return CopyGenericsFieldWise(template);
            } else if (objType.IsArray) {
                return CopyArrayFieldWise((Array) template, objType.GetElementType());
            } else {
                return CopySingleValueFieldWise(template, objType);
            }
        }

        private static object CopyArrayFieldWise<T>(T array, Type objType) {
            Array typedArray = (Array) (object) array;
            if (objType.IsPrimitive || objType == typeof(string)) {
                return typedArray.Clone();
            }

            int arrayRank = typedArray.Rank;

            if (arrayRank > 1) {
                var lengths = new int [arrayRank];
                int elementsSum = 0;
                for (int index = 0; index < arrayRank; ++index) {
                    lengths [index] = typedArray.GetLength(index);
                    if (index == 0) {
                        elementsSum = lengths [index];
                    } else {
                        elementsSum *= lengths [index];
                    }
                }

                Array arrayCopy = Array.CreateInstance(objType, lengths);

                var coordinates = new int [arrayRank];
                for (int i = 0; i < elementsSum; ++i) {
                    // Determine the index for each of the array's dimensions
                    int currentIndex = i;
                    for (int dimensionIndex = arrayRank - 1; dimensionIndex >= 0; --dimensionIndex) {
                        coordinates [dimensionIndex] = currentIndex % lengths [dimensionIndex];
                        currentIndex /= lengths [dimensionIndex];
                    }

                    // Clone the current array element
                    object originalElement = typedArray.GetValue(coordinates);
                    if (originalElement != null) {
                        arrayCopy.SetValue(CopyFieldWise(originalElement, originalElement.GetType()), coordinates);
                    }

                }
                return arrayCopy;
            } else {
                Array arrayCopy = Array.CreateInstance(objType, typedArray.Length);
                object [] arrayObj = array as object [];

                for (int i = 0; i < typedArray.Length; i++) {
                    object originalValue = typedArray.GetValue(i);
                    if (originalValue != null) {
                        arrayCopy.SetValue(CopyFieldWise(originalValue, originalValue.GetType()), i);
                    }
                }
                return arrayCopy;
            }
        }

        private static object CopyGenericsFieldWise<T>(T originalCollection) {
            Type objType = originalCollection.GetType();
            Type [] typeArgs = originalCollection.GetType().GetGenericArguments();
            if (typeof(IDictionary).IsAssignableFrom(objType)) {
                object newDictionary;
                IDictionary dict = (IDictionary) originalCollection;
                if (objType.IsGenericTypeDefinition) {
                    Type dictionaryType = typeof(Dictionary<,>);
                    Type constructedType = dictionaryType.MakeGenericType(typeArgs);
                    newDictionary = (IDictionary) Activator.CreateInstance(constructedType);
                } else {
                    Type typeDefinition = objType.GetGenericTypeDefinition();
                    Type constructedType = typeDefinition.MakeGenericType(typeArgs);
                    newDictionary = (T) Activator.CreateInstance(constructedType);
                }

                foreach (DictionaryEntry entry in dict) {
                    var value = CopyFieldWise(entry.Value, entry.Value.GetType());
                    ((IDictionary) newDictionary).Add(entry.Key, value);
                }

                return (T) newDictionary;
                ;
            } else if ((typeof(IList).IsAssignableFrom(objType))) {
                IList list = (IList) originalCollection;
                Type listType = typeof(List<>);

                Type constructedType = listType.MakeGenericType(typeArgs);
                IList newList = (IList) Activator.CreateInstance(constructedType);

                foreach (var entry in list) {
                    newList.Add(CopyFieldWise(entry, typeArgs [0]));
                }

                return (T) newList;
            } else {
                Type tupleType = Type.GetType("System.Tuple`" + typeArgs.Length);
                Type constructedType = tupleType.MakeGenericType(typeArgs);

                FieldInfo [] fieldInfos = Helpers.GetFieldInfos(objType, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

                var valuesList = new List<object>();
                foreach (var fieldInfo in fieldInfos) {
                    var value = fieldInfo.GetValue(originalCollection);
                    var copy = CopyFieldWise(value, value.GetType());
                    valuesList.Add(copy);
                }

                return (T) Activator.CreateInstance(constructedType, valuesList.ToArray());
            }
        }

    }
}

