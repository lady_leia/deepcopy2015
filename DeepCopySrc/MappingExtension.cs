﻿using DeepCopySrc.src;
using System;
using System.Collections.Generic;

namespace DeepCopySrc
{
    public static class MappingExtension
    {
        public static T CopyFrom<T>(this T source, T template) {
            source = ReflectionFieldWiseCopy.Copy(source, template);
            return source;
        }

        public static void ForEach<T>(this IEnumerable<T> @this, Action<T, int> action) {
            int index = 0;
            foreach (T item in @this) {
                action(item, index);
                index++;
            }
        }


    }
}