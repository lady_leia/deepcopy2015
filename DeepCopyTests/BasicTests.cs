﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DeepCopyTests.src;
using DeepCopySrc;
using System.Linq;
using System.Collections.Generic;
using DeepCopyTestClasses;

namespace DeepCopyTests
{
	[TestClass]
	public class BasicTests
	{
		[TestMethod]
		public void ClassWithDefaultConstructor()
		{
			ClassWithDefaultConstructor c1 = new ClassWithDefaultConstructor(45, "string1", false);
			var c2 = c1.CopyFrom(c1);

			AreEqualNotSame(c1, c2);
		}

		[TestMethod]
		public void ListOfPrimitives()
		{
			var a = new List<int> { 1, 2, 3, 4, 5 };

			var a2 = a.CopyFrom(a);

			Assert.AreNotSame(a, a2);
			a.ForEach((o, i) => Assert.AreEqual(o, a2 [i]));
		}

		[TestMethod]
		public void ArrayOfPrimitives()
		{
			var a = new int [] { 1, 2, 3, 4, 5 };

			var a2 = a.CopyFrom(a);

			Assert.AreNotSame(a, a2);
			a.ForEach((o, i) => Assert.AreEqual(o, a2 [i]));
		}

		[TestMethod]
		public void ArrayOfObjects()
		{
			var a = new int [] { 1, 2, 3, 4, 5 };
			var b = a.Select(o => CreateTestObjectInstance(o, "string" + o, true)).ToArray();
			var b2 = b.CopyFrom(b);

			Assert.AreNotSame(b, b2);
			b2.ForEach((o, i) => AreEqualNotSame(o, b.ElementAt(i)));
		}

		[TestMethod]
		public void SimpleDictionary()
		{
			IDictionary<int, string> dict = new Dictionary<int, string>();

			for (int i = 10; i < 15; i++)
			{
				dict.Add(i, "dictString" + 1);
			}

			var dict2 = dict.CopyFrom(dict);
			Assert.AreNotSame(dict, dict2);
		}

		[TestMethod]
		public void SimpleTuple()
		{
			Tuple<int, string, string> t = new Tuple<int, string, string>(1, "s1", "s2");
			var t2 = t.CopyFrom(t);

			Assert.AreNotSame(t, t2);
			Assert.AreEqual(t, t2);
		}

		[TestMethod]
		public void ObjectTuple()
		{
			Tuple<SimpleClass, ModerateClass> t = new Tuple<SimpleClass, ModerateClass>(SimpleClass.CreateForTests(45), ModerateClass.CreateForTests(34));
			var t2 = t.CopyFrom(t);

			Assert.AreNotSame(t, t2);
			SimpleClassTests.Assert_AreEqualButNotSame(t.Item1, t2.Item1);
			ModerateClassTests.Assert_AreEqualButNotSame(t.Item2, t2.Item2);
		}

		private void AreEqualNotSame(ClassWithDefaultConstructor o1, ClassWithDefaultConstructor o2)
		{
			Assert.AreNotSame(o1, o2);
			Assert.AreEqual(o1.PublicField, o2.PublicField);
			Assert.AreEqual(o1.PublicReadOnlyField, o2.PublicReadOnlyField);
			Assert.AreEqual(o1.GetPrivateField(), o2.GetPrivateField());
			Assert.AreEqual(o1.GetPrivateProperty(), o2.GetPrivateProperty());
			Assert.AreEqual(o1.StringValue, o2.StringValue);
		}

		private ClassWithDefaultConstructor CreateTestObjectInstance(int i, string s, bool b)
		{
			return new ClassWithDefaultConstructor(i, s, b);
		}
	}
}
