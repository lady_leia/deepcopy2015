﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DeepCopyTests.src;
using DeepCopySrc;
using System.Linq;

namespace DeepCopyTests
{
	[TestClass]
	public class ClassWithoutDefaultConstructorTests
	{
		[TestMethod]
		public void SimpleClass()
		{
			ClassWithoutDefaultConstructor c1 = new ClassWithoutDefaultConstructor(45, "string1", false);
			var c2 = c1.CopyFrom(c1);

			AreEqualNotSame(c1, c2);
		}

		[TestMethod]
		public void ArrayOfObjects()
		{
			var a = new int [] { 1, 2, 3, 4, 5 };
			var b = a.Select(o => CreateTestObjectInstance(o, "string" + o, true)).ToArray();
			var b2 = b.CopyFrom(b);

			Assert.AreNotSame(b, b2);
			b2.ForEach((o, i) => AreEqualNotSame(o, b.ElementAt(i)));
		}

		private void AreEqualNotSame(ClassWithoutDefaultConstructor o1, ClassWithoutDefaultConstructor o2)
		{
			Assert.AreNotSame(o1, o2);
			Assert.AreEqual(o1.PublicField, o2.PublicField);
			Assert.AreEqual(o1.PublicReadOnlyField, o2.PublicReadOnlyField);
			Assert.AreEqual(o1.GetPrivateField(), o2.GetPrivateField());
			Assert.AreEqual(o1.GetPrivateProperty(), o2.GetPrivateProperty());
			Assert.AreEqual(o1.StringValue, o2.StringValue);
		}

		private ClassWithoutDefaultConstructor CreateTestObjectInstance(int i, string s, bool b)
		{
			return new ClassWithoutDefaultConstructor(i, s, b);
		}
	}
}
