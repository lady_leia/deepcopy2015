namespace DeepCopyTests.src
{
	public class ClassWithoutDefaultConstructor
	{

		/// <summary>
		///   Initializes a new instance of the class without default constructor
		/// </summary>
		/// <param name="intValue">intValue value that will be saved by the instance</param>
		public ClassWithoutDefaultConstructor(int i, string s, bool b)
		{
			privateField = i * 10 + 10;
			StringValue = s;
			ProtectedString = s + i;
			BoolValue = b;
			PublicReadOnlyField = i * 100 + 100;
			PublicField = s + s + s;
		}

		//Properties
		public string StringValue { get; set; }
		private bool BoolValue { get; set; }
		protected string ProtectedString { get; set; }

		//Fields
		public readonly int PublicReadOnlyField;
		private int privateField;
		public string PublicField;

		public int GetPrivateField()
		{
			return privateField;
		}

		public bool GetPrivateProperty()
		{
			return BoolValue;
		}
	}
}