﻿using System;
using DeepCopySrc;

namespace DeepCopyTests
{
	public static class CopyFunctionSelection
	{
		public static Func<object, object> CopyMethod;

		static CopyFunctionSelection()
		{
			CopyMethod = (obj) => obj.CopyFrom(obj);
		}
	}
}
